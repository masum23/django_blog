# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import author, category, article, comment


# Register your models here.


class authorModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["name", "details"]

    class Meta:
        Model = author


admin.site.register(author, authorModel)


class articleModel(admin.ModelAdmin):
    list_display = ["__str__","posted_on"]
    search_fields = ["title", "article_author", "details"]
    list_per_page = 15
    list_filter = ["posted_on","category"]

    class Meta:
        Model = article


admin.site.register(article, articleModel)


class categoryModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["name"]
    list_per_page = 10
    list_filter = ["name"]

    class Meta:
        Model = category


admin.site.register(category, categoryModel)


class commentModel(admin.ModelAdmin):
    list_display = ["__str__"]
    search_fields = ["name", "email"]
    list_per_page = 20
    list_filter = ["name", "email"]

    class Meta:
        Model = comment


admin.site.register(comment, commentModel)
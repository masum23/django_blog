from django.shortcuts import render, HttpResponse, get_object_or_404, redirect
from .models import author, category, article, comment
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from .forms import createForm, registerUser, createAuthor, commentForm


# Create your views here.
def index(request):
    post = article.objects.all()
    search = request.GET.get('q')
    if search:
        post = post.filter(
            Q(title__icontains=search) |
            Q(body__icontains=search)

        )
    paginator = Paginator(post, 6)  # Show 8 post per page

    page = request.GET.get('page')
    total_article = paginator.get_page(page)
    context = {
        "post": total_article
    }
    return render(request, "index.html", context)


def getauthor(request, name):
    post_author = get_object_or_404(User, username=name)
    auth = get_object_or_404(author, name=post_author.id)
    post = article.objects.filter(article_author=auth.id)
    paginator = Paginator(post, 6)  # Show 25 contacts per page

    page = request.GET.get('page')
    total_article = paginator.get_page(page)
    context = {
        "auth": auth,
        "post": total_article
    }
    return render(request, "profile.html", context)


def getsingle(request, id):
    post = get_object_or_404(article, pk=id)
    first = article.objects.first()
    last = article.objects.last()
    getComment = comment.objects.filter(post=id)
    related = article.objects.filter(category=post.category).exclude(id=id)[:4]
    form = commentForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.post = post
        instance.save()

    context = {
        "post": post,
        "first": first,
        "last": last,
        "related": related,
        "form": form,
        "comment":getComment
    }
    return render(request, "single.html", context)


def getTopic(request, name):
    cat = get_object_or_404(category, name=name)
    post = article.objects.filter(category=cat.id)
    paginator = Paginator(post, 8)  # Show 25 contacts per page

    page = request.GET.get('page')
    total_article = paginator.get_page(page)
    return render(request, "category.html", {"post": total_article, "cat": cat})


def getLogin(request):
    if request.user.is_authenticated:
        return redirect('index')
    else:
        if request.method == "POST":
            user = request.POST.get('user')
            password = request.POST.get('pass')
            auth = authenticate(request, username=user, password=password)
            if auth is not None:
                login(request, auth)
                return redirect('index')
            else:
                messages.add_message(request, messages.ERROR, 'User name or password mismatch')
    return render(request, "login.html")


def getlogout(request):
    logout(request)
    return redirect('index')


def getcreate(request):
    if request.user.is_authenticated:
        u = get_object_or_404(author, name=request.user.id)
        form = createForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.article_author = u
            instance.save()
            messages.success(request, 'Article Added Successfully')
            return redirect('profile')
        return render(request, 'create.html', {"form": form})
    else:
        messages.add_message(request, messages.INFO, 'Please login to add article')
        return redirect('login')


def getUpdate(request,id):
    if request.user.is_authenticated:
        u = get_object_or_404(author, name=request.user.id)
        post=get_object_or_404(article, id=id)
        form = createForm(request.POST or None, request.FILES or None, instance=post)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.article_author = u
            instance.save()
            messages.success(request, 'Article Updated Successfully')
            return redirect('profile')
        return render(request, 'create.html', {"form": form})
    else:
        messages.add_message(request, messages.INFO, 'Please login to add article')
        return redirect('login')

def getDelete(request,id):
    if request.user.is_authenticated:
        post=get_object_or_404(article, id=id)
        post.delete()
        messages.warning(request, 'Article Deleted Successfully')
        return redirect('profile')
    else:
        return redirect('login')


def getProfile(request):
    if request.user.is_authenticated:
        user = get_object_or_404(User, id=request.user.id)
        author_profile = author.objects.filter(name=user.id)
        if author_profile:
            author_User = get_object_or_404(author, name=request.user.id)
            post = article.objects.filter(article_author=author_User.id)
            return render(request, 'logged_in_profile.html', {"post": post, "user": author_User})
        else:
            form = createAuthor(request.POST or None, request.FILES or None)
            if form.is_valid():
                instance = form.save(commit=False)
                instance.name = user
                instance.save()
                messages.success(request, 'Author profile is created successfully')
                return redirect('profile')
            return render(request, 'createauthor.html',{"form":form})

    else:
        return redirect('login')


def getRegister(request):
    form = registerUser(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request,'Registration successfully completed')
        return  redirect('login')
    return render(request, 'register.html',{"form":form})
